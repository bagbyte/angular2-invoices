declare var moment:any;

export module Entities {
    export class Menu {
        area: string;
        children: MenuItem[];

        constructor (area: string, children: MenuItem[]) {
            this.area = area;
            this.children = children;
        }
        
        hasChildren() {
            return ((this.children != null) && (this.children.length > 0));
        }
    }

    export class MenuItem {
        name: string;
        icon: string;
        link: string;
        children: MenuItem[];

        constructor (name: string, icon: string, link: string, children: MenuItem[]) {
            this.name = name;
            this.icon = icon;
            this.link = link;
            this.children = children;
        }

        hasChildren() {
            return ((this.children != null) && (this.children.length > 0));
        }
    }
    
    export class Expense {
        id: number;
        amount: number = 0;
        hasInvoice: boolean = false;
        note: string;
        description: string;
        
        isAmountValid() {
            return (this.amount != null) && !isNaN(this.amount)
        }

        isValid() {
            return this.isAmountValid();
        }
/*
        constructor (json) {
            this.amount = json.amount;
            this.hasInvoice = json.hasInvoice ? true : false;
            this.note = json.note;
            this.description = json.description;
        }
*/
        static fromMap(map) {
            var expense = new Expense()

            expense.id = map.id ? map.id : null
            expense.amount = map.amount ? map.amount : null
            expense.hasInvoice = map.hasInvoice ? map.hasInvoice : false
            expense.note = map.note ? map.note : null
            expense.description = map.description ? map.description : null

            return expense
        }
    }

    export class Report {
        id: number;
        date: string;
        total: number = 0;
        cash: number = 0;
        note: string;
        expenses: Expense[] = [];

        static pad(s) {
            return (s < 10) ? '0' + s : s;
        }

        constructor() {
            this.date = Report.currentDate();
        }

        static currentDate() {
            var d = new Date();
            return [Report.pad(d.getDate()), Report.pad(d.getMonth()+1), d.getFullYear()].join('/');
        }

        isDateValid() {
            return moment(this.date, "DD/MM/YYYY", true).isValid();
        }

        isTotalValid() {
            return !isNaN(this.total)
        }

        isCashValid() {
            return !isNaN(this.cash)
        }

        isNoteValid() {
            return true;
        }

        areExpensisValid() {
            this.expenses.forEach(function(exp: Expense) {
                if (!exp.isValid())
                    return false;
            });
            return true;
        }

        isValid() {
            return this.isDateValid() && this.isTotalValid() && this.isCashValid() && this.isNoteValid() && this.areExpensisValid();
        }
/*
        constructor (json) {
            this.date = json.date;
            this.total = json.total;
            this.cash = json.cash;
            this.note = json.note;
            this.expenses = json.expenses;
        }
*/
        extra = function() {
            var extra = 0;

            if (!isNaN(this.total))
                extra = this.total;

            if (!isNaN(this.cash))
                extra -= this.cash;

            this.expenses.forEach(function(expense: Expense) {
                if (!isNaN(expense.amount))
                    extra -= expense.amount;
            });

            return extra;
        };

        hasExpenses(): boolean {
            return ((this.expenses != null) && (this.expenses.length > 0));
        }

        static fromMap(map) {
            var report = new Report();
            var date = Report.currentDate();

            if (map.date) {
                let d = new Date(map.date);
                date = [Report.pad(d.getDate()), Report.pad(d.getMonth()+1), d.getFullYear()].join('/');
//                var pattern = /(\d{2})\.(\d{2})\.(\d{4})/;
//                date = date.replace(pattern,'$3-$2-$1')

            }

            report.id = map.id ? map.id : null;
            report.date = date;
            report.total = map.total ? map.total : 0;
            report.cash = map.cash ? map.cash : 0;
            report.note = map.note ? map.note : null;

            if (map.expenses) {
                report.expenses = [];
                var expenses = report.expenses;
                map.expenses.forEach(function(expMap) {
                    report.expenses.push(Expense.fromMap(expMap))
                });
            }

            return report
        }

        toJSON() {
            var json = {
                total: this.total,
                cash: this.cash
            };

            if (this.date)
                json['date'] = this.date.substring(6,10) + '-' + this.date.substring(3,5) + '-' + this.date.substring(0,2) + 'T10:00:00Z';

            if (this.note)
                json['note'] = this.note;

            if (this.expenses)
                json['expenses'] = this.expenses;

            return JSON.stringify(json);
        }
    }
}