import { bootstrap }    from '@angular/platform-browser-dynamic';
import { AppComponent } from './app/components/app.component';
import { ROUTER_PROVIDERS, ROUTER_DIRECTIVES } from '@angular/router';
import { HTTP_PROVIDERS } from '@angular/http';
import { Configuration } from './app/app.constants';

bootstrap(AppComponent, [Configuration, ROUTER_PROVIDERS, ROUTER_DIRECTIVES, HTTP_PROVIDERS ]);