export class Pagination {
    limit: number = 10;
    page: number = 0;
    total: number = 0;

    /**
     * Array of values for populating the srop down with the number of items to display per page
     */
    getResultsPerPage() {
        return [10, 25, 50, 100]
    }

    /**
     * Starting item of the page (Showing items from startingItem() till endingItem() of maxRecords())
     */
    startingItem() {
        return this.offset() + 1
    }

    /**
     * Ending item of the page (Showing items from startingItem() till endingItem() of maxRecords())
     */
    endingItem() {
        return Math.min(this.startingItem() + this.limit, this.total);
    }

    /**
     * Max records in the database (Showing items from startingItem() till endingItem() of maxRecords())
     */
    maxRecords() {
        return this.total
    }

    /**
     * Return the last page of the pagination
     */
    maxPages() {
        let num = this.maxRecords() / this.limit
        return (num > Math.round(num)) ? Math.round(num)+1 : num;
    }

    /**
     * False is the Previous button has to appear disabled
     */
    previousAllowed() {
        return this.page > 0;
    }

    /**
     * False is the Next button has to appear disabled
     */
    nextAllowed() {
        return this.page < this.maxPages();
    }

    /**
     * False is the page p has to appear disabled
     */
    isCurrentPage(p) {
        return (p == this.page);
    }

    offset() {
        return this.page * this.limit
    }
}