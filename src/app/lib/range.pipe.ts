import { Pipe, PipeTransform } from '@angular/core';

/*
 * Transform a number into a range.
 * Usage:
 *   value | range:start
 * Example:
 *   {{ 2 |  range:0}}
 *   returns: [0, 1, 2]
 */
@Pipe({name: 'range'})

export class RangePipe implements PipeTransform {
    transform(value: number, start: number = 0): number[] {
        var arr: number[] = [];

        for (var _i = start; _i < value; _i++)
            arr.push(_i);

        return arr;
    }
}