import { Component } from '@angular/core';
import { CORE_DIRECTIVES } from '@angular/common';
import { Entities } from '../../modules/entities.module';
import { ReportService } from '../../services/report.service';
import { RouteSegment, Router, ROUTER_PROVIDERS, ROUTER_DIRECTIVES} from '@angular/router';
import { Pagination } from '../../lib/pagination'
import { RangePipe } from '../../lib/range.pipe'

@Component({
    selector: 'reports-list',
    providers: [ReportService, ROUTER_DIRECTIVES],
    directives: [CORE_DIRECTIVES],
    pipes: [RangePipe],
    templateUrl: 'src/app/components/reports/list.html'
})
export class ReportsListComponent {
    reports: Entities.Report[] = [];

    sortingColumn: string = 'date';
    sortingAsc: boolean = true;

    pagination: Pagination = new Pagination();

    constructor(private _reportService: ReportService,
                private _params: RouteSegment,
                private _router: Router) { }

    ngOnInit() {
        this.getReports()
    }

    parseResponse(data) {
        var component = this;
        component.pagination.total = data.total;

        component.reports = [];
        data.results.forEach(function(report) {
            component.reports.push(Entities.Report.fromMap(report))
        });
    }

    getReports() {
        var component = this;
        let router = this._router;

        let params = {
            max: this.pagination.limit,
            offset: this.pagination.offset(),
            sort: this.sortingColumn,
            order: this.sortingAsc ? 'asc' : 'desc'
        };

        this._reportService.GetAll(params)
            .subscribe(data => component.parseResponse(data),
                error => router.navigate(['/reports/list']),
                () => {}
            );
    }
    
    isColumnSorted(column: string) : boolean {
        return (this.sortingColumn == column)
    }

    changeSorting(column: string) {
        if (this.sortingColumn == column) {
            this.sortingAsc = !this.sortingAsc;
        }
        else {
            this.sortingColumn = column;
            this.sortingAsc = true;
        }
        
        this.getReports();
    }

    classForColumn(column: string) : string {
        if (this.isColumnSorted(column)) {
            if (this.sortingAsc)
                return 'sorting_asc';
            else
                return 'sorting_desc';
        }
        return 'sorting';
    }

    changePage(p) {
        if (!this.pagination.isCurrentPage(p)) {
            this.pagination.page = p;
            this.getReports();
        }
    }
}