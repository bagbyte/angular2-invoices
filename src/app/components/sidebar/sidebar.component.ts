import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import { Entities } from '../../modules/entities.module';
import 'rxjs/Rx';

declare var $:any;

@Component({
    selector: 'sidebar',
    directives: [ROUTER_DIRECTIVES],
    template: `
    <div class="col-md-3 left_col">

        <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
                <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Dashboard</span></a>
            </div>
    
            <div class="clearfix"></div>
    
            <!-- menu profile quick info -->
            <div class="profile">
                <div class="profile_pic">
                    <img src="src/assets/images/img.jpg" alt="..." class="img-circle profile_img">
                </div>
                <div class="profile_info">
                    <span>Welcome,</span>
                    <h2>John Doe</h2>
                </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu" *ngIf="menuTree">
                <div class="menu_section" *ngFor="let menuAreas of menuTree">
                    <h3>{{ menuAreas.area }}</h3>
                    <ul class="nav side-menu" *ngIf="menuAreas.hasChildren()">
                        <li *ngFor="let menu of menuAreas.children" class="menu"><a [href]="[menu.link ? menu.link : '']"><i class="fa {{ menu.icon }}" *ngIf="menu.icon"></i> {{ menu.name }} <span class="fa fa-chevron-down" *ngIf="menu.hasChildren()"></span></a>
                            <ul class="nav child_menu" *ngIf="menu.hasChildren()">
                                <li *ngFor="let submenu of menu.children" class="submenu"><a [routerLink]="[submenu.link ? submenu.link : '']">{{ submenu.name }}</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            
            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                <a data-toggle="tooltip" data-placement="top" title="Settings">
                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                    <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="Lock">
                    <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                </a>
                <a data-toggle="tooltip" data-placement="top" title="Logout">
                    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                </a>
            </div>
            <!-- /menu footer buttons -->
        </div>
    `

})
export class SidebarComponent {
    menuTree: Entities.Menu[] = [
        new Entities.Menu('General', [
            new Entities.MenuItem('Dashboard', 'fa-tachometer', '/', []),
            new Entities.MenuItem('Daily Report', 'fa-calendar-check-o', null, [
                new Entities.MenuItem('List', '', '/reports/list', []),
                new Entities.MenuItem('Add', '', '/reports/create', [])
            ]),
            new Entities.MenuItem('Suppliers', 'fa-truck', null, [
                new Entities.MenuItem('List', '', '/suppliers/list', []),
                new Entities.MenuItem('Add', '', '/suppliers/create', [])
            ]),
            new Entities.MenuItem('Products', 'fa-th', null, [
                new Entities.MenuItem('List', '', '/products/list', []),
                new Entities.MenuItem('Add', '', '/products/create', [])
            ]),
            new Entities.MenuItem('Invoices', 'fa-file-text', null, [
                new Entities.MenuItem('List', '', '/invoices/list', []),
                new Entities.MenuItem('Add', '', '/invoices/create', [])
            ])
        ]),
        new Entities.Menu('Admin area', [
            new Entities.MenuItem('Users', 'fa-users', null, [
                new Entities.MenuItem('List', '', '/users/list', []),
                new Entities.MenuItem('Add', '', '/users/create', [])
            ])
        ])
    ];

    ngAfterViewInit() {
        this.activateControllers();
    }

    activateControllers() {
        var $BODY = $('body'),
            $MENU_TOGGLE = $('#menu_toggle'),
            $SIDEBAR_MENU = $('#sidebar-menu'),
            $SIDEBAR_FOOTER = $('.sidebar-footer'),
            $LEFT_COL = $('.left_col'),
            $RIGHT_COL = $('.right_col'),
            $NAV_MENU = $('.nav_menu'),
            $FOOTER = $('footer');

        var setContentHeight = function () {
            // reset height
            $RIGHT_COL.css('min-height', $(window).height());

            var bodyHeight = $BODY.height(),
                leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
                contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

            // normalize content
            contentHeight -= $NAV_MENU.height() + $FOOTER.height();

            $RIGHT_COL.css('min-height', contentHeight);
        };

        // prevent page load on links without href
        $SIDEBAR_MENU.find('a[href=""]').on('click', function(ev) {
            ev.preventDefault();
        });

        $SIDEBAR_MENU.find('a').on('click', function(ev) {
            var $li = $(this).parent();
            var isChildMenuItem = $li.parent().is('.child_menu');

            if (isChildMenuItem) {
                $('.child_menu li').removeClass('active');
                $li.addClass('active');
            } else {
                if (!$li.hasClass('active')) {
                    $SIDEBAR_MENU.find('.side-menu > li').removeClass('active');
                    $SIDEBAR_MENU.find('li ul').slideUp();

                    $li.addClass('active');

                    $('ul:first', $li).slideDown(function() {
                        setContentHeight();
                    });
                }
                else {
                    $SIDEBAR_MENU.find('li ul').slideUp();

                    $li.removeClass('active');
                }
            }
        });

        $SIDEBAR_MENU.find('a.router-link-active').parent('li').addClass('active').parents('ul').slideDown(function() {
            setContentHeight();
        }).parent().addClass('active');

        // fixed sidebar
        if ($.fn.mCustomScrollbar) {
            $('.menu_fixed').mCustomScrollbar({
                autoHideScrollbar: true,
                theme: 'minimal',
                mouseWheel:{ preventDefault: true }
            });
        }
    }
}