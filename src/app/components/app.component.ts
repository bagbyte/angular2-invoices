import { Component } from '@angular/core';
import { Routes, ROUTER_DIRECTIVES } from '@angular/router';
import { SidebarComponent } from './sidebar/sidebar.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { ReportsListComponent } from './reports/reports-list.component'
import { ReportsEditComponent } from './reports/reports-edit.component'

@Component({
    selector: 'app',
    directives: [ROUTER_DIRECTIVES, SidebarComponent, DashboardComponent, ReportsListComponent, ReportsEditComponent],
    template: `
        <div class="container body">
          <div class="main_container">
            <sidebar></sidebar>
            
            <top-nav></top-nav>
            
            <router-outlet></router-outlet>
            
            <footer>
              <div class="pull-right">
                Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
              </div>
              <div class="clearfix"></div>
            </footer>
          </div>
        </div>
    `
})
@Routes([
    { path: '/', component: DashboardComponent },
    { path: '/reports/list', component: ReportsListComponent },
    { path: '/reports/create', component: ReportsEditComponent },
    { path: '/reports/edit/:id', component: ReportsEditComponent },
    { path: '/suppliers/list', component: ReportsListComponent },
    { path: '/suppliers/create', component: ReportsEditComponent }
])
export class AppComponent {

}