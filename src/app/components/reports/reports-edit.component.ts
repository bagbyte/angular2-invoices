import { Component } from '@angular/core';
import { CORE_DIRECTIVES } from '@angular/common';
import { Entities } from '../../modules/entities.module';
import { ReportService } from '../../services/report.service';
import { RouteSegment, Router, ROUTER_PROVIDERS, ROUTER_DIRECTIVES} from '@angular/router';

declare var $:any;

@Component({
    selector: 'reports-create',
    providers: [ReportService, ROUTER_DIRECTIVES],
    directives: [CORE_DIRECTIVES],
    templateUrl: 'src/app/components/reports/edit.html'
})
export class ReportsEditComponent {
    status: string;
    report: Entities.Report = new Entities.Report();
    expense: Entities.Expense = new Entities.Expense();
    newExpense: boolean = true;
    expenseToDelete: number;

    success: string;
    info: string;
    warning: string;
    danger: string;

    constructor(private _reportService: ReportService,
                private _params: RouteSegment,
                private _router: Router) { }

    ngOnInit() {
        var component = this;
        let id: string = this._params.getParam('id');
        let router: Router = this._router;

        if (id) {
            this._reportService.GetSingle(Number(id))
                .subscribe(data => component.report = Entities.Report.fromMap(data),
                    error => router.navigate(['/reports/list']),
                    () => {});
        }
    }

    ngAfterViewInit() {
        $('#report-date').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_4",
            format: 'DD/MM/YYYY'
        });
    }

    emptyExpense() {
        this.expense = new Entities.Expense();
        this.newExpense = true;
    };

    addExpense() {
        if (this.expense.isValid()) {
            if (this.newExpense)
                this.report.expenses.push(this.expense);
        }
    };

    deleteExpense(index) {
        if (this.expenseToDelete != null) {
            this.report.expenses.splice(index, 1);

            this.unsetExpenseToDelete();
        }
    };

    setCurrentExpense(index) {
        this.newExpense = false;
        this.expense = this.report.expenses[index];
    };

    setExpenseToDelete(index) {
        this.expenseToDelete = index;
    };

    unsetExpenseToDelete() {
        this.expenseToDelete = null;
    };

    saveReport() {
        var component = this;
        if (this.report.isValid()) {
            if (this.report.id == null) {
                this._reportService
                    .Add(this.report)
                    .subscribe(data => {
                        this.report = Entities.Report.fromMap(data);
                        this._router.navigate(['/reports/edit/' + this.report.id ])
                    },
                        error => this.danger = error,
                        () => this.success = 'Report created');
            }
            else {
                this._reportService
                    .Update(this.report.id, this.report)
                    .subscribe(data => this.report = Entities.Report.fromMap(data),
                        error => this.danger = error,
                        () => this.success = 'Report saved');
            }
        }
    }

    closeAlert() {
        this.success = null;
        this.info = null;
        this.warning = null;
        this.danger = null;
    }
}